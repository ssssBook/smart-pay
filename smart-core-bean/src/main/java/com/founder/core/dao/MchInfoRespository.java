package com.founder.core.dao;

import com.founder.core.domain.MchInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public interface MchInfoRespository extends JpaRepository<MchInfo, String>, JpaSpecificationExecutor<MchInfo> {
}
